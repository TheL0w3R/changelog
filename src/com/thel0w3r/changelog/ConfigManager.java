package com.thel0w3r.changelog;

import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.file.YamlConfigurationOptions;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by TheL0w3R on 18/06/2015.
 * All Rights Reserved.
 */
public class ConfigManager {

    private YamlConfiguration cfg;
    private YamlConfigurationOptions cfgOptions;
    private YamlConfiguration logs;
    private YamlConfigurationOptions logsOptions;
    private JavaPlugin pl;

    private HashMap<String, Object> loadedCfg;
    private HashMap<String, List<String>> loadedLogs;

    public ConfigManager() {
        cfg = new YamlConfiguration();
        cfgOptions = cfg.options();
        logs = new YamlConfiguration();
        logsOptions = logs.options();
        this.pl = ChangeLog.getInstance();
        loadedCfg = new HashMap<>();
        loadedLogs = new HashMap<>();
        initConfig();
        reloadConfig();
    }

    public void initConfig() {
        cfgOptions.header("##################################\n" +
                "#      ChangeLog config file     #\n" +
                "#          by: TheL0w3R          #\n" +
                "##################################\n");

        logsOptions.header("##################################\n" +
                "#       ChangeLog log file       #\n" +
                "#          by: TheL0w3R          #\n" +
                "##################################\n");

        cfg.set("general.plugin_prefix", "&8[&6HPWizard&8]&f ");
        cfg.set("commands.doesnotexist", "&cThis command doesn't exists!");
        cfg.set("commands.notplayer", "&cHPWizard can only be used by a player!");
        cfg.set("commands.nopermissions", "&cYou don't have permissions to use this command!");

        logs.set("logs", new String[]{});

        if(!(new File(pl.getDataFolder() + "/config.yml").exists())) {
            try {
                cfg.save(pl.getDataFolder() + "/config.yml");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(!(new File(pl.getDataFolder() + "/logs.yml").exists())) {
            try {
                logs.save(pl.getDataFolder() + "/logs.yml");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void modifyLog(String value) {
        List<String> tmp = loadedLogs.get("logs");
        for(String str : tmp) {
            System.out.println(str);
        }
        tmp.add(value);
        logs.set("logs", tmp);
        try {
            logs.save(pl.getDataFolder() + "/logs.yml");
        } catch (IOException e) {
            e.printStackTrace();
        }
        reloadConfig();
    }

    public void reloadConfig() {
        try {
            //System.out.println("Loading " + pl.getDataFolder() + "/config.yml");
            cfg.load(pl.getDataFolder() + "/config.yml");
            logs.load(pl.getDataFolder() + "/logs.yml");
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
        loadedCfg.clear();
        loadedCfg.put("general.plugin_prefix", ChatColor.translateAlternateColorCodes('&', cfg.getString("general.plugin_prefix")));
        loadedCfg.put("commands.doesnotexist", ChatColor.translateAlternateColorCodes('&', cfg.getString("commands.doesnotexist")));
        loadedCfg.put("commands.notplayer", ChatColor.translateAlternateColorCodes('&', cfg.getString("commands.notplayer")));
        loadedCfg.put("commands.nopermissions", ChatColor.translateAlternateColorCodes('&', cfg.getString("commands.nopermissions")));

        loadedLogs.clear();
        loadedLogs.put("logs", logs.getStringList("logs"));
    }

    public List<String> getLogs(String path) {
        return loadedLogs.get(path);
    }

    public Object getValue(String path) {
        return loadedCfg.get(path);
    }
}
