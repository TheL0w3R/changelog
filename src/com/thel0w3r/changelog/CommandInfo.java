package com.thel0w3r.changelog;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by TheL0w3R on 18/06/2015.
 * All Rights Reserved.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CommandInfo {
    String description() default "";
    String usage() default "";
    String[] aliases() default "";
    boolean onlyPlayer();
}
