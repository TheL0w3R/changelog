package com.thel0w3r.changelog.cmds;

import com.thel0w3r.changelog.CommandInfo;
import com.thel0w3r.changelog.ConfigManager;
import com.thel0w3r.changelog.GameCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by TheL0w3R on 12/06/2016.
 * All Rights Reserved.
 */
@CommandInfo(description = "Show a list of commits.", usage = "", aliases = {"c", "check"}, onlyPlayer = false)
public class Check extends GameCommand {

    private ConfigManager cm;

    public Check(ConfigManager cm) {
        this.cm = cm;
    }

    @Override
    public void onCommand(Player p, String[] args) {

    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if(!sender.isOp()) {
            return;
        }

        sender.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "*************" + ChatColor.YELLOW + "" + ChatColor.BOLD + "ChangeLog Entry List" + ChatColor.GOLD + "" + ChatColor.BOLD + "*************");
        for(String str : cm.getLogs("logs")) {
            String[] splitted = str.split(":");
            String tmp = ChatColor.BLUE + "" + ChatColor.BOLD + splitted[0] + ":" + ChatColor.GRAY + splitted[1];
            sender.sendMessage(tmp);
        }
        sender.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "*************" + ChatColor.YELLOW + "" + ChatColor.BOLD + "ChangeLog Entry List" + ChatColor.GOLD + "" + ChatColor.BOLD + "*************");
    }
}
