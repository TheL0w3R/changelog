package com.thel0w3r.changelog.cmds;

import com.thel0w3r.changelog.CommandInfo;
import com.thel0w3r.changelog.GameCommand;
import com.thel0w3r.changelog.prompts.LogFactory;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by TheL0w3R on 12/06/2016.
 * All Rights Reserved.
 */
@CommandInfo(description = "Write a log and prepare to commit.", usage = "", aliases = {"l", "log"}, onlyPlayer = true)
public class Log extends GameCommand {

    private LogFactory lf;

    public Log(LogFactory lf) {
        this.lf = lf;
    }

    @Override
    public void onCommand(Player p, String[] args) {
        lf.convfact.buildConversation(p).begin();
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {

    }
}
