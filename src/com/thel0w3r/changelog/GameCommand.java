package com.thel0w3r.changelog;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by TheL0w3R on 18/06/2015.
 * All Rights Reserved.
 */
public abstract class GameCommand {

    public abstract void onCommand(Player p, String[] args);
    public abstract void onCommand(CommandSender sender, String[] args);

}
