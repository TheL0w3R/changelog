package com.thel0w3r.changelog.prompts;

import com.thel0w3r.changelog.ChangeLog;
import com.thel0w3r.changelog.ConfigManager;
import com.thel0w3r.changelog.util.ColorUtil;
import org.bukkit.ChatColor;
import org.bukkit.conversations.*;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by TheL0w3R on 12/06/2016.
 * All Rights Reserved.
 */
public class LogFactory implements ConversationAbandonedListener, ColorUtil {

    public ConversationFactory convfact;
    private HashMap<UUID, String> playersWriting;
    private ConfigManager cfg;

    public LogFactory(ConfigManager cfg) {
        convfact = new ConversationFactory(ChangeLog.getInstance())
                .withModality(false)
                .withLocalEcho(true)
                .withFirstPrompt(new LogPrompt())
                .withTimeout(3600)
                .thatExcludesNonPlayersWithMessage("Only players can log changes!")
                .addConversationAbandonedListener(this);
        playersWriting = new HashMap<>();
        this.cfg = cfg;
    }

    @Override
    public void conversationAbandoned(ConversationAbandonedEvent conversationAbandonedEvent) {

    }

    public class LogPrompt extends StringPrompt {

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return GOLD + "" + BOLD + "Write the log message below, ++ to keep adding text to what you wrote before, \"commit\" to save changes \"cancel\" to return.";
        }

        @Override
        public Prompt acceptInput(ConversationContext conversationContext, String s) {
            Player p = (Player)conversationContext.getForWhom();
            if(s.equalsIgnoreCase("cancel")) {
                p.sendRawMessage(GOLD + "Exited without saving!");
                return Prompt.END_OF_CONVERSATION;
            } else if(s.equalsIgnoreCase("commit")) {
                cfg.modifyLog(p.getName() + ":" + playersWriting.get(p.getUniqueId()));
                p.sendRawMessage(GOLD + "Commited changes! Now admins can review it by \"/log check\"");
                return Prompt.END_OF_CONVERSATION;
            }

            if(playersWriting.containsKey(p.getUniqueId())) {
                if(s.startsWith("++")) {
                    String temp = playersWriting.get(p.getUniqueId());
                    temp += s.substring(2);
                    playersWriting.put(p.getUniqueId(), temp);
                } else {
                    playersWriting.put(p.getUniqueId(), s);
                }
            } else {
                playersWriting.put(p.getUniqueId(), s);
            }

            return new LogPrompt();
        }
    }
}
