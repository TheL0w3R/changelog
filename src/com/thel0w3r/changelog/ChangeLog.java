package com.thel0w3r.changelog;

import com.thel0w3r.changelog.prompts.LogFactory;
import javafx.scene.control.TextFormatter;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by TheL0w3R on 11/06/2016.
 * All Rights Reserved.
 */
public class ChangeLog extends JavaPlugin {

    private static ChangeLog instance;
    private ConfigManager cm;
    private CommandManager comm;
    private LogFactory lf;

    @Override
    public void onEnable() {
        instance = this;
        cm = new ConfigManager();
        lf = new LogFactory(cm);
        comm = new CommandManager(cm, lf);

        getCommand("log").setExecutor(comm);
    }

    public static ChangeLog getInstance() {
        return instance;
    }

}
