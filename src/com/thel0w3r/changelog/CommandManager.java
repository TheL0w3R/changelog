package com.thel0w3r.changelog;

import com.thel0w3r.changelog.cmds.Check;
import com.thel0w3r.changelog.cmds.Log;
import com.thel0w3r.changelog.prompts.LogFactory;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by TheL0w3R on 18/06/2015.
 * All Rights Reserved.
 */
public class CommandManager implements CommandExecutor {

    private ArrayList<GameCommand> cmds;
    private ConfigManager configManager;

    public CommandManager(ConfigManager configManager, LogFactory lf) {
        this.configManager = configManager;
        cmds = new ArrayList<>();
        cmds.add(new Log(lf));
        cmds.add(new Check(configManager));
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

        if(!sender.isOp()) {
            return true;
        }

        if (cmd.getName().equalsIgnoreCase("l") || cmd.getName().equalsIgnoreCase("log")) {
            if (args.length == 0) {
                sender.sendMessage(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "*************" + ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "ChangeLog Command List" + ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "*************");
                for (GameCommand gcmd : cmds) {
                    CommandInfo info = gcmd.getClass().getAnnotation(CommandInfo.class);
                    sender.sendMessage(ChatColor.GRAY + "" + ChatColor.BOLD + "> " + ChatColor.RED + "" + ChatColor.BOLD + "/log " + ChatColor.BLUE + "" + ChatColor.BOLD + info.aliases()[0] + " " + ChatColor.GRAY + info.usage() + ChatColor.WHITE + "" + ChatColor.BOLD + " - " + ChatColor.GOLD + info.description());
                }
                sender.sendMessage(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "*************" + ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "ChangeLog Command List" + ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "*************");
                return true;
            }

            GameCommand wanted = null;

            for (GameCommand gcmd : cmds) {
                CommandInfo info = gcmd.getClass().getAnnotation(CommandInfo.class);
                for(String alias : info.aliases()) {
                    if (alias.equals(args[0])){
                        wanted = gcmd;
                        break;
                    }
                }
            }

            if (wanted == null) {
                sender.sendMessage(configManager.getValue("general.plugin_prefix") + (String) configManager.getValue("commands.doesnotexist"));
                return true;
            }

            ArrayList<String> newArgs = new ArrayList<>();
            Collections.addAll(newArgs, args);
            newArgs.remove(0);
            args = newArgs.toArray(new String[newArgs.size()]);

            if(wanted.getClass().getAnnotation(CommandInfo.class).onlyPlayer()) {
                if(!(sender instanceof Player)) {
                    sender.sendMessage(configManager.getValue("general.plugin_prefix") + (String) configManager.getValue("commands.notplayer"));
                    return true;
                }
                Player p = (Player) sender;
                wanted.onCommand(p, args);
            } else {
                wanted.onCommand(sender, args);
            }
        }

        return true;
    }
}
